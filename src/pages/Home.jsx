import { Box, CircularProgress, Typography } from "@mui/material";

import React, { useEffect, useState } from "react";
import Board from "../components/boards/Board";
import Navbar from "../components/navbar/Navbar";
import CreateBoard from "../components/boards/CreateBoard";
import { getData } from "../fetchData";
import { asyncErrorHandler } from "../utils/asyncHandler";
import { useMyContext } from "../MyContext";
import { useNavigate } from "react-router-dom";

function Home() {
  const { setSnackbarOpen, setErrMessage } = useMyContext();
  const [boards, setBoards] = useState([]);
  const [loader, setLoader] = useState(true);
  const navigate = useNavigate();
  const id = import.meta.env.VITE_MY_ID;

  useEffect(() => {
    async function getMyData() {
      const res = await asyncErrorHandler(
        getData,
        "try again later",
        setSnackbarOpen,
        "organizations",
        id,
        "boards"
      );
      console.log(res);
      if (!res) {
        setErrMessage(() => "error while loading the boards");
        navigate("/error");
      }else{
        setBoards(res);
      }
      setLoader(false);
    }
    getMyData();
  }, []);

  return (
    <>
      {loader ? (
        <Box
          sx={{
            width: "100%",
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <CircularProgress />
        </Box>
      ) : (
        <Box
          sx={{
            height: "100%",
            width: "100%",
            bgcolor: "white",
            pl: "39rem",
            pr: "13rem",
            boxSizing: "border-box",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Navbar />
          <Box
            sx={{
              height: "40rem",
              width: "100%",
              bgcolor: "aliceblue",
              mt: 1,
              display: "flex",
              flexWrap: "wrap",
              alignContent: "flex-start",
              gap: 1,
              boxSizing: "border-box",
              borderRadius: "0.5rem",
              py: 7,
              px: 4,
              position: "relative",
              textAlign: "center",
            }}
          >
            {boards?.length > 0 ? (
              boards?.map((board) => {
                return <Board board={board} key={board.id} />;
              })
            ) : (
              <Typography
                sx={{
                  fontSize: "2rem",
                  textAlign: "center",
                }}
              >
                There are no boards, you can create one
              </Typography>
            )}
            <Box
              sx={{
                bgcolor: "#d0e1f080",
                position: "absolute",
                width: "22rem",
                height: "30rem",
                top: "12%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "column",
                left: "-24rem",
              }}
            >
              <CreateBoard setBoards={setBoards} />
            </Box>
          </Box>
        </Box>
      )}
    </>
  );
}

export default Home;
