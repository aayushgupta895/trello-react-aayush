import {
  Box,
  Button,
  CircularProgress,
  LinearProgress,
  TextField,
} from "@mui/material";
import React, { useEffect } from "react";
import { useState } from "react";
import { Outlet, useNavigate, useParams } from "react-router-dom";
import { deleteTheList, getData, postList } from "../fetchData";
import List from "../components/listAndCards/List";
import { grey } from "@mui/material/colors";
import { asyncErrorHandler } from "../utils/asyncHandler";
import { useMyContext } from "../MyContext";

function BoardDetail() {
  const { id } = useParams();
  const [lists, setlists] = useState([]);
  const [listInput, setListInput] = useState("");
  const [displayListInput, setDisplayListInput] = useState(false);
  const [loader, setLoader] = useState(true);
  const [actionLoader, setActionLoader] = useState(false);
  const navigate = useNavigate();
  const { setSnackbarOpen, setErrMessage } = useMyContext();

  useEffect(() => {
    async function getlists() {
      const data = await asyncErrorHandler(
        getData,
        "some error occurred",
        setSnackbarOpen,
        "boards",
        id,
        "lists"
      );
      if (!data) {
        setErrMessage(() => "error while loading the list");
        navigate("/error");
      } else {
        setlists(data);
      }
      setLoader(false);
    }
    getlists();
  }, []);

  async function deleteList(listId) {
    setActionLoader(true);
    const data = await asyncErrorHandler(
      deleteTheList,
      "error while deleting the list",
      setSnackbarOpen,
      listId
    );
    if (data) {
      setlists((prev) => prev.filter((element) => element.id !== listId));
    }
    setActionLoader(false);
  }

  async function addList(e) {
    e.preventDefault();
    setActionLoader(true);
    if (displayListInput && (listInput == "" || listInput.trim() == "")) {
      setSnackbarOpen({
        open: true,
        message: "cannot create with empty list",
      });
    } else {
      if (displayListInput && listInput !== "") {
        const res = await asyncErrorHandler(
          postList,
          "cannot post list! try again later",
          setSnackbarOpen,
          listInput,
          id
        );
        if (res) {
          setlists((prev) => [...prev, res]);
        }
      }
    }
    setActionLoader(false);
    setDisplayListInput(!displayListInput);
    setListInput("");
  }

  return (
    <>
      {loader ? (
        <Box
          sx={{
            width: "100%",
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <CircularProgress />
        </Box>
      ) : (
        <Box
          sx={{
            width: "100%",
            height: "100%",
          }}
        >
          {actionLoader ? (
            <Box
              sx={{
                mt: 0.1,
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <LinearProgress
                color="success"
                sx={{
                  width: "100%",
                  height: "4px",
                }}
              />
            </Box>
          ) : (
            <></>
          )}
          <Box
            sx={{
              width: "100%",
              height: "100%",
              boxSizing: "border-box",
              overflow: "scroll",
              px: 10,
              py: 4,

              bgcolor: grey[300],
              display: "-webkit-box",
              flexDirection: "column",
              gap: "1rem",
            }}
          >
            {lists?.map((data, index) => {
              return (
                <List
                  key={data.id}
                  data={data}
                  listId={data.id}
                  deleteList={deleteList}
                />
              );
            })}
            <Box
              sx={{
                height: "fit-content",
                width: "27rem",
                overflowWrap: "break-word",
                bgcolor: "#F1F2F4",
                borderRadius: "0.5rem",
                p: 2,
              }}
            >
              <form onSubmit={addList}>
                <TextField
                  variant="standard"
                  InputProps={{
                    disableUnderline: true,
                  }}
                  onChange={(e) => setListInput(e.target.value)}
                  value={listInput}
                  sx={{
                    textAlign: "start",
                    bgcolor: "#FFFFFF",
                    outline: "none",
                    border: "none",
                    width: "100%",
                    mt: 0.5,
                    display: displayListInput ? "block" : "none",
                    px: 1,
                    py: 0.4,
                    boxSizing: "border-box",
                    backgroundColor: "rgb(255, 255, 255)",
                    borderRadius: " 8px",
                    boxShadow: `rgba(9, 30, 66, 0.25) 0px 1px 1px 0px, rgba(9, 30, 66, 0.31) 0px 0px 1px 0px`,
                  }}
                />
                <Button
                  variant="contained"
                  type="submit"
                  sx={{
                    width: "70%",
                    mt: 0.7,
                  }}
                >
                  {displayListInput ? "save list" : "add list"}
                </Button>
              </form>
            </Box>
            <Outlet />
          </Box>
        </Box>
      )}
    </>
  );
}

export default BoardDetail;
