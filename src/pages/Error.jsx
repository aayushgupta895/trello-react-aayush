import React from "react";
import { useMyContext } from "../MyContext";
import Alert from "@mui/material/Alert";
import { Stack } from "@mui/material";

function Error() {
  const { errMessage } = useMyContext();
  return (
    <Stack sx={{ width: "100%" }} spacing={2}>
      <Alert severity="error">{errMessage}</Alert>
    </Stack>
  );
}

export default Error;
