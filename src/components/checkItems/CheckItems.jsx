import {
  Box,
  Button,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  LinearProgress,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import DeleteIcon from "@mui/icons-material/Delete";
import {
  createItem,
  deleteTheCheckItem,
  updateCheckItems,
} from "../../fetchData";
import { orange } from "@mui/material/colors";
import { asyncErrorHandler } from "../../utils/asyncHandler";
import { useMyContext } from "../../MyContext";

function CheckItems(props) {
  const { setSnackbarOpen } = useMyContext();
  const { id, setCheckItemsData, checkItemsData } = props;
  const [checkItemHeader, setCheckItemHeader] = useState("");
  const [loader, setLoader] = useState(false);
  const { cardId } = useParams();

  async function createCheckItems(e) {
    e.preventDefault();
    setLoader(true);
    if (checkItemHeader == "" || checkItemHeader.trim() == "") {
      setSnackbarOpen({
        open: true,
        message: "could not create empty item",
      });
    } else {
      const res = await asyncErrorHandler(
        createItem,
        "could not create check item",
        setSnackbarOpen,
        "checklists",
        id,
        "checkItems",
        checkItemHeader
      );
      if (res) {
        setCheckItemsData((prev) => [...prev, res]);
      }
    }
    setCheckItemHeader("");
    setLoader(false);
  }

  async function deleteCheckItem(itemId) {
    setLoader(true);

    const res = await asyncErrorHandler(
      deleteTheCheckItem,
      "could not delete checkItem",
      setSnackbarOpen,
      id,
      itemId
    );
    if (res) {
      setCheckItemsData((prev) => {
        return prev.filter((data) => {
          return data.id != itemId;
        });
      });
    }
    setLoader(false);
  }

  const checkBoxChange = async (data) => {
    setLoader(true);
    const newState = data.state == "complete" ? "incomplete" : "complete";

    const res = await asyncErrorHandler(
      updateCheckItems,
      "could not update! try again later",
      setSnackbarOpen,
      cardId,
      data.id,
      newState
    );
    if (res)
      setCheckItemsData((prev) => {
        return prev.map((each) => {
          if (each.id == data.id) {
            return {
              ...each,
              state: newState,
            };
          }
          return each;
        });
      });
    setLoader(false);
  };

  return (
    <>
      {loader ? (
        <Box
          sx={{
            mt: 0.1,
            width: "100%",
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <LinearProgress
            color="success"
            sx={{
              width: "100%",
              height: "4px",
            }}
          />
        </Box>
      ) : (
        <></>
      )}
      <Box
        sx={{
          width: 400,
          outline: "none",
          borderRadius: "0.5rem",
          p: 1.2,
        }}
      >
        <Box
          sx={{
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "column",
            gap: 0.9,
            alignItems: "center",
            mt: 1.2,
          }}
        >
          {checkItemsData?.map((data) => {
            return (
              <Box
                key={data.id}
                sx={{
                  width: "100%",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <Box
                  sx={{
                    width: "100%",
                    height: "100%",
                    borderRadius: 1.3,
                    bgcolor: "#1a90ff15",
                    overflow: "hidden",
                    display: "flex",
                    pl: 1,
                  }}
                >
                  <FormControlLabel
                    key={data.id}
                    sx={{
                      flex: "0.1",
                      mr: 0,
                    }}
                    control={
                      <Checkbox
                        checked={data.state == "complete"}
                        onChange={() => {
                          checkBoxChange(data);
                        }}
                      />
                    }
                  />
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      pr: 0.8,
                      flex: "0.9",
                    }}
                  >
                    <Typography
                      sx={{
                        px: 0.5,
                        py: 0.5,
                      }}
                    >
                      {data.name}
                    </Typography>
                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        gap: "2px",
                      }}
                    >
                      <DeleteIcon
                        onClick={() => deleteCheckItem(data.id)}
                        sx={{
                          width: "1.8rem",
                          height: "1.2rem",
                          cursor: "pointer",
                          color: orange[500],
                        }}
                      />
                    </Box>
                  </Box>
                </Box>
              </Box>
            );
          })}
        </Box>
        <form
          onSubmit={createCheckItems}
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            gap: "0.5rem",
            marginBottom: "0.8rem",
            marginTop: "20px",
          }}
        >
          <TextField
            label="create checkItems here"
            variant="outlined"
            sx={{
              m: 0,
            }}
            value={checkItemHeader}
            onChange={(e) => setCheckItemHeader(e.target.value)}
            fullWidth
            margin="normal"
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            sx={{
              px: 0.9,
              py: 0.7,
              fontSize: "0.85rem",
            }}
          >
            Submit
          </Button>
        </form>
      </Box>
    </>
  );
}

export default CheckItems;
