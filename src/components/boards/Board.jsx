import { Box } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";

function Board(props) {
  const navigate = useNavigate();
  const { board } = props;

  return (
    <Box
      onClick={() => {
        navigate(`/board/${board.id}`);
      }}
      sx={{
        cursor: "pointer",
        width: "14rem",
        height: "8rem",
        borderRadius: "0.5rem",
        background: board?.prefs.backgroundImage
          ? `url(${board?.prefs.backgroundImage})`
          : board?.prefs.backgroundColor,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {board.name}
    </Box>
  );
}

export default Board;
