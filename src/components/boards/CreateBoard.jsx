import {
  Box,
  Button,
  LinearProgress,
  Snackbar,
  TextField,
} from "@mui/material";
import React, { useState } from "react";
import { postData, setBoardBackgrounds } from "../../fetchData";
import { boardsColorBackground } from "../../assests/boardsBackgrounds";
import { asyncErrorHandler } from "../../utils/asyncHandler";
import { useMyContext } from "../../MyContext";

function CreateBoard(props) {
  const { setBoards } = props;
  const { setSnackbarOpen } = useMyContext();
  const [boardName, setBoardName] = useState("");
  const [boardBackground, setBoardBackground] = useState("sky");
  const [loader, setLoader] = useState(false);

  async function createBoard(e) {
    e.preventDefault();
    setLoader(true);
    if (boardName == "" || boardName.trim() == "") {
      setSnackbarOpen({
        open: true,
        message: "cannot create with empty value",
      });
    } else {
      const data = await asyncErrorHandler(
        postData,
        "some error occurred",
        setSnackbarOpen,
        "boards",
        `name=${boardName}`
      );
      if (!data) {
        const res = await asyncErrorHandler(
          setBoardBackgrounds,
          "some error occurred while setting background",
          setSnackbarOpen,
          data.id,
          boardBackground
        );
        if (res) {
          data.prefs.backgroundColor = res.prefs.backgroundColor;
          setBoards((prev) => [...prev, data]);
        }
      }
    }
    setLoader(false);
  }

  return (
    <Box
      className="createBoard"
      component="div"
      sx={{
        width: "90%",
        display: "flex",
        gap: "1rem",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Box
        className="preview"
        sx={{
          background: boardsColorBackground[boardBackground],
          backgroundSize: "cover",
          backgroundPosition: "center",
          width: "61%",
          height: "9rem",
          // mb: 1,
          borderRadius: "0.2rem",
        }}
      >
        <Box
          component="img"
          sx={{
            pt: 1,
          }}
          src="https://trello.com/assets/14cda5dc635d1f13bc48.svg"
        ></Box>
      </Box>
      <Box
        sx={{
          width: "90%",
          height: "3rem",
          display: "flex",
          gap: "0.5rem",
        }}
      >
        {Object.keys(boardsColorBackground).map((bg) => {
          return (
            <Box
              onClick={() => setBoardBackground(bg)}
              key={bg}
              sx={{
                background: boardsColorBackground[bg],
                height: "100%",
                flex: 1,
                backgroundSize: "cover",
                backgroundPosition: "center",
                borderRadius: "0.4rem",
                cursor: "pointer",
              }}
            ></Box>
          );
        })}
      </Box>
      {loader ? (
        <Box
          sx={{
            mt: 0.1,
            width: "100%",
            // height: "0%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <LinearProgress
            color="success"
            sx={{
              width: "100%",
              height: "4px",
            }}
          />
        </Box>
      ) : (
        <></>
      )}
      <form onSubmit={createBoard}>
        <TextField
          id="outlined-basic"
          label="name the board"
          variant="outlined"
          sx={{
            width: "90%",
            mt: 0.5,
          }}
          type="text"
          value={boardName}
          onChange={(e) => setBoardName(e.target.value)}
        />
        <Button
          variant="contained"
          type="submit"
          // onClick={createBoard}
          sx={{
            mt: 0.5,
            width: "90%",
          }}
        >
          Create
        </Button>
      </form>
    </Box>
  );
}

export default CreateBoard;
