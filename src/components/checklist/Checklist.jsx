import React, { useEffect, useState } from "react";
import { getData } from "../../fetchData";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import { blue, orange } from "@mui/material/colors";
import styled from "@emotion/styled";
import { Box, Typography } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import CheckItems from "../checkItems/CheckItems";
import { asyncErrorHandler } from "../../utils/asyncHandler";
import { useMyContext } from "../../MyContext";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";

function Checklist({ data, deleteChecklist }) {
  const [displayItems, setDisplayItems] = useState(false);
  const [checkItemsData, setCheckItemsData] = useState([]);
  const { setSnackbarOpen } = useMyContext();

  useEffect(() => {
    async function getTheCheckItems() {
      const res = await asyncErrorHandler(
        getData,
        "could not load items data",
        setSnackbarOpen,
        "checklists",
        data.id,
        "checkItems"
      );
      if (!res) return;
      setCheckItemsData(res);
    }
    getTheCheckItems();
  }, []);

  const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor:
        theme.palette.grey[theme.palette.mode === "light" ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      backgroundColor: theme.palette.mode === "light" ? blue[500] : "#308fe8",
    },
  }));

  let completed = 0;
  checkItemsData.forEach((each) => {
    if (each.state == "complete") {
      completed++;
    }
  });


  return (
    <Box key={data.id} sx={{ width: "100%", display: "checkItem" }}>
      <Box
        sx={{
          width: "100%",
          height: "100%",
          borderRadius: 1.3,
          bgcolor: "#1a90ff15",
          mt: 0.9,
          overflow: "hidden",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            pr: 0.8,
          }}
        >
          <Box
            sx={{
              width: "85%",
              display: "flex",
              justifyContent: "start",
              alignItems: "center",
              cursor: "pointer",
            }}
            onClick={() => setDisplayItems(!displayItems)}
          >
            {displayItems && <ArrowDropDownIcon />}
            {!displayItems && <ArrowDropUpIcon />}
            <Typography
              sx={{
                px: 0.5,
                py: 0.5,
                cursor: "pointer",
                width : '90%',
                wordWrap : 'break-word'
              }}
            >
              {data.name}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              gap: "2px",
            }}
          >
            <Typography
              sx={{
                fontSize: "0.9rem",
                pt: 0.2,
              }}
            >
              {completed}/{checkItemsData.length}
            </Typography>
            <DeleteIcon
              onClick={() => deleteChecklist(data.id)}
              sx={{
                width: "1.8rem",
                height: "1.2rem",
                cursor: "pointer",
                color: orange[500],
              }}
            />
          </Box>
        </Box>
        <BorderLinearProgress
          variant="determinate"
          sx={{
            width: "100%",
          }}
          value={(completed / checkItemsData.length) * 100}
        />
        {displayItems && (
          <CheckItems
            id={data.id}
            data={data}
            setCheckItemsData={setCheckItemsData}
            checkItemsData={checkItemsData}
          />
        )}
      </Box>
    </Box>
  );
}

export default Checklist;
