import {
  Box,
  Button,
  CircularProgress,
  LinearProgress,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  deleteTheChecklist,
  getCheckList,
  postCheckList,
} from "../../fetchData";
import { useMyContext } from "../../MyContext";

import Checklist from "./Checklist";
import { asyncErrorHandler } from "../../utils/asyncHandler";

function ChecklistWrapper() {
  const [open, setOpen] = useState(true);
  const [checkListHeader, setCheckListHeader] = useState("");
  const [checkListData, setCheckListData] = useState([]);
  const { setSnackbarOpen } = useMyContext();
  const navigate = useNavigate();
  const { cardId } = useParams();
  const [loader, setLoader] = useState(true);
  const [listLoader, setListLoader] = useState(false);

  useEffect(() => {
    async function getCheckListData() {
      const res = await asyncErrorHandler(
        getCheckList,
        "could not load checklist",
        setSnackbarOpen,
        cardId
      );
      if (res) {
        setCheckListData(res);
      }
      setLoader(false);
    }
    getCheckListData();
  }, []);

  async function deleteChecklist(id) {
    setListLoader(true);
    const res = await asyncErrorHandler(
      deleteTheChecklist,
      "could not delete checklist",
      setSnackbarOpen,
      id
    );
    if (res)
      setCheckListData((prev) =>
        prev.filter((data) => {
          return data.id !== id;
        })
      );
    setListLoader(false);
  }

  function handleClose() {
    setOpen(false);
    navigate(-1);
  }

  async function createCheckList(e) {
    e.preventDefault();
    setListLoader(true);

    if (checkListHeader == "" || checkListHeader.trim() == "") {
      setSnackbarOpen({
        open: true,
        message: "give it a name",
      });
    } else {
      const data = await asyncErrorHandler(
        postCheckList,
        "could not create list",
        setSnackbarOpen,
        checkListHeader,
        cardId
      );
      if (data) {
        setCheckListData((prev) => [...prev, data]);
        setCheckListHeader("");
      }
    }
    setListLoader(false);
  }

  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
        aria-labelledby="parent-modal-title"
        aria-describedby="parent-modal-description"
      >
        {loader ? (
          <Box
            sx={{
              width: "100%",
              height: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <CircularProgress color="info" thickness={5} />
          </Box>
        ) : (
          <Box
            sx={{
              height: "fit-content",
              maxHeight: "500px",
              overflow: "scroll",
              bgcolor: "background.paper",
              outline: "none",
              borderRadius: "0.5rem",
              boxShadow: 24,
              p: 2,
              width: "479px",
            }}
          >
            <form
              onSubmit={createCheckList}
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                gap: "0.5rem",
                marginBottom: "0.8rem",
              }}
            >
              <TextField
                label="Enter checkList"
                variant="outlined"
                sx={{
                  m: 0,
                }}
                value={checkListHeader}
                onChange={(e) => setCheckListHeader(e.target.value)}
                fullWidth
                margin="normal"
              />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                sx={{
                  px: 0.9,
                  py: 0.7,
                  fontSize: "0.85rem",
                }}
              >
                Submit
              </Button>
            </form>
            {listLoader ? (
              <Box
                sx={{
                  mt: 0.1,
                  width: "100%",
                  height: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <LinearProgress
                  color="success"
                  sx={{
                    width: "100%",
                    height: "4px",
                  }}
                />
              </Box>
            ) : (
              <></>
            )}
            <Box
              sx={{
                width: "100%",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              {checkListData?.length > 0 ? (
                checkListData.map((data, index) => {
                  return (
                    <Checklist
                      data={data}
                      key={data.id}
                      deleteChecklist={deleteChecklist}
                    />
                  );
                })
              ) : (
                <Typography sx={{ mt: 2 }}>No checklist</Typography>
              )}
            </Box>
          </Box>
        )}
      </Modal>
    </>
  );
}

export default ChecklistWrapper;
