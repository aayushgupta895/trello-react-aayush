import {
  Box,
  Typography,
  Button,
  TextField,
  Snackbar,
  LinearProgress,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import {
  deleteTheCard,
  getData,
  postCard,
} from "../../fetchData";
import DeleteIcon from "@mui/icons-material/Delete";
import Cards from "./Cards";
import { orange } from "@mui/material/colors";
import { asyncErrorHandler } from "../../utils/asyncHandler";
import { useMyContext } from "../../MyContext";

function List(props) {
  const { data, deleteList } = props;
  const [displayInput, setDisplayInput] = useState(false);
  const [cardInput, setCardInput] = useState("");
  const { setSnackbarOpen } = useMyContext();
  const [cards, setCards] = useState([]);
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    async function getCards() {
      const res = await asyncErrorHandler(
        getData,
        "could not load cards",
        setSnackbarOpen,
        "lists",
        data.id,
        "cards"
      );
      if(res){
        setCards(res);
      }
      setLoader(false);
    }
    getCards();
  }, []);

  async function addCard(e) {
    e.preventDefault();
    setLoader(true);
    if (displayInput && (cardInput == "" || cardInput.trim() == "")) {
      setSnackbarOpen({
        open: true,
        message: "cannot create with empty value",
      });
      setDisplayInput(!displayInput);
    } else if (displayInput && cardInput !== "") {
      const res = await asyncErrorHandler(
        postCard,
        "error occurred while saving the card",
        setSnackbarOpen,
        data.id,
        cardInput
      );
      if (res)
      setCards((prev) => [...prev, res]);
    }
    setDisplayInput(!displayInput);
    setLoader(false);
    setCardInput("");
  }

  async function deleteCard(id) {
    setLoader(true);
    const data = await asyncErrorHandler(
      deleteTheCard,
      "could not delete the card! try again later",
      setSnackbarOpen,
      id
    );
    if (data) setCards((prev) => prev.filter((each) => each.id != id));
    setLoader(false);
  }

  return (
    <Box
      sx={{
        height: "fit-content",
        width: "17rem",
        overflowWrap: "break-word",
        bgcolor: "#F1F2F4",
        borderRadius: "0.5rem",
        py: 0.75,
        px: 0.6875,
      }}
    >
      <Box
        sx={{
          display: "flex",
        }}
      >
        <Typography
          sx={{
            fontWeight: "700",
            width: "100%",
            textAlign: "start",
            paddingLeft: "0.2rem",
            fontSize: "1.1rem",
            ml: 1,
          }}
        >
          {data.name}
        </Typography>
        <Button onClick={() => deleteList(data.id)}>
          <DeleteIcon
            sx={{
              width: "2.8rem",
              height: "1.3rem",
              color: orange[500],
            }}
          />
        </Button>
      </Box>
      {loader ? (
        <Box
          sx={{
            mt: 0.1,
            width: "100%",
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <LinearProgress
            color="success"
            sx={{
              width: "100%",
              height: "4px",
            }}
          />
        </Box>
      ) : (
        <></>
      )}
      {cards?.map((card) => {
        return <Cards card={card} key={card.id} deleteCard={deleteCard} />;
      })}
      <form onSubmit={addCard}>
        <TextField
          variant="standard"
          onKeyDown={(e) => e.stopPropagation()}
          InputProps={{
            disableUnderline: true,
          }}
          onChange={(e) => setCardInput(e.target.value)}
          value={cardInput}
          sx={{
            textAlign: "start",
            bgcolor: "#FFFFFF",
            outline: "none",
            border: "none",
            width: "100%",
            mt: 0.5,
            display: displayInput ? "block" : "none",
            px: 1,
            py: 0.4,
            boxSizing: "border-box",
            backgroundColor: "rgb(255, 255, 255)",
            borderRadius: " 8px",
            boxShadow: `rgba(9, 30, 66, 0.25) 0px 1px 1px 0px, rgba(9, 30, 66, 0.31) 0px 0px 1px 0px`,
          }}
        />
        <Button
          variant="contained"
          type="submit"
          sx={{
            // width: "70%",
            mt: 0.7,
            ml: 0.5,
            width: "7.5rem",
            height: "2rem",
            display: "flex",
          }}
        >
          {displayInput ? "save task" : "add task"}
        </Button>
      </form>
    </Box>
  );
}

export default List;
