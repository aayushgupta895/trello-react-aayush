import { Box, Button, Typography } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import React from "react";

import { orange } from "@mui/material/colors";
import { useNavigate } from "react-router-dom";


function Cards(props) {
  
  const navigate = useNavigate();
  const { card, deleteCard } = props;
  return (
    <Box
      sx={{
        width: "100%",
        backgroundColor: "rgb(255, 255, 255)",
        borderRadius: "8px",
        overflow: "hidden",
        mt: 0.5,
        display: "flex",
        position: "relative",
        boxShadow: `rgba(9, 30, 66, 0.25) 0px 1px 1px 0px, rgba(9, 30, 66, 0.31) 0px 0px 1px 0px`,
      }}
    >
      <Typography
        key={card.id}
        onClick={()=>navigate(`card/${card.id}`)}
        sx={{
          cursor: "pointer",
          textAlign: "start",
          bgcolor: "#FFFFFF",
          // mt: 0.5,
          width: "85%",
          px: 1,
          py: 0.4,
          boxSizing: "border-box",
        }}
      >
        {card.name}
      </Typography>
      <Button
        sx={{
          position: "absolute",
          top: "0.4375rem",
          right: 0,
        }}
        onClick={() => deleteCard(card.id)}
      >
        <DeleteIcon
          sx={{
            width: "1.8rem",
            height: "1.1rem",
            color: orange[500],
          }}
        />
      </Button>
    </Box>
  );
}

export default Cards;
