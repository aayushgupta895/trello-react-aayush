import { Box, Typography } from "@mui/material";
import React from "react";

function Navbar({name}) {
  const header = name || "boards"
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        height: "4rem",
        bgcolor: "#d0e1f080",
        mt : 0.5,
        borderRadius : "0.5rem",
        width : '100%'
      }}
    >
      <Typography
        sx={{
          color: "black",
          fontSize: "1.5rem",
          ml: 4,
          fontWeight: "700",
        }}
        color="hsl(200deg 82.16% 28.62%)"
        >
        {header}
      </Typography>
      <Box
        sx={{
          ":hover": {
            backgroundImage:
              "url(	https://trello.com/assets/87e1af770a49ce8e84e3.gif)",
          },
          backgroundImage:
            "url(	https://trello.com/assets/d947df93bc055849898e.gif)",
          height: "100%",
          width: "6rem",
          ml: 4,
          backgroundRepeat: "no-repeat",
          backgroundSize: "contain",
          backgroundPosition: "center",
          display: "block",
          filter:
            "brightness(0) saturate(100%) invert(30%) sepia(53%) saturate(323%) hue-rotate(179deg) brightness(91%) contrast(88%)",
        }}
      ></Box>
    </Box>
  );
}

export default Navbar;
