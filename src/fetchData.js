import axios from "axios";

axios.defaults.params = {
  key: import.meta.env.VITE_API,
  token: import.meta.env.VITE_TOKEN,
};


export async function getData(searchFrom, id, searchItem) {
  try {
    const res = await axios.get(
      `https://api.trello.com/1/${searchFrom}/${id}/${searchItem}`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    return res.data;
  } catch (error) {
    console.log(`error is `, error);
    throw new Error(error);
  }
}

export async function postData(to, info) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/${to}/?${info}`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    return response.data;
  } catch (error) {
    console.log(`error is `, error);
    throw new Error(error);
  }
}

export async function postCard(listId, name) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/cards?idList=${listId}&name=${name}`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    return response.data;
  } catch (error) {
    console.log(`error is `, error);
    throw new Error(error);
  }
}

export async function createItem(location, id, item, name) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/${location}/${id}/${item}?name=${name}`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    return response.data;
  } catch (error) {
    console.log(`error is `, error);
    throw new Error(error);
  }
}

export async function postList(name, boardId) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/lists?name=${name}&idBoard=${boardId}`, // info would be name=to do
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function deleteTheList(listId) {
  try {
    const response = await axios.put(
      `https://api.trello.com/1/lists/${listId}/closed?value=true` 
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function deleteTheCard(cardId) {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/cards/${cardId}`, // info would be name=to do
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    return response;
  } catch (error) {
    throw new Error(error);
  }
}

export async function deleteTheChecklist(checklistId) {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/checklists/${checklistId}`, // info would be name=to do
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    return response;
  } catch (error) {
    throw new Error(error);
  }
}

export async function deleteTheCheckItem(checklistId, checkItemId) {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}`, // info would be name=to do
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    return response;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getCheckList(id) {
  try {
    const res = await axios.get(
      `https://api.trello.com/1/cards/${id}/checklists`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    return res.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function postCheckList(name, cardId) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/checklists?name=${name}&idCard=${cardId}`, // info would be name=to do
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export const setBoardBackgrounds = async (boardId, backgroundUrl) => {
  try {
    const res = await axios.put(
      `https://api.trello.com/1/boards/${boardId}/prefs/background`,
      {
        value: backgroundUrl,
      }
    );
    return res.data;
  } catch (error) {
    throw new Error(error);
  }
};

export const updateCheckItems = async (cardId, checkItemId, status) => {
  try {
    const res = await axios.put(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?&state=${status}`
    );
    return res.data;
  } catch (error) {
    throw new Error(error);
  }
};

//api.trello.com/1/checklists?idCard=5abbe4b7ddc1b351ef961414&key=APIKey&token=APIToken
