
export async function asyncErrorHandler(func, message, setSnackbarOpen, ...args) {
  try {
    const res = await func(...args);
    return res;
  } catch (error) {
    setSnackbarOpen({
      open : true,
      message : message
    })
    console.error(`An error occurred: ${error}`);
  }
}
