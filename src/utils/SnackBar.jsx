import { Alert, Snackbar } from "@mui/material";
import React from "react";

function SnackBar(props) {
  const { open, setOpen } = props;
  function snackBarClose() {
    const obj = {
      open: false,
      message: "",
    };
    setOpen(obj);
  }
  return (
    <Snackbar
      open={open.open}
      autoHideDuration={2000}
      onClose={snackBarClose}
    >
      <Alert
        onClose={snackBarClose}
        severity="warning"
        variant="filled"
        sx={{ width: "100%" }}
      >
        {open.message}
      </Alert>
    </Snackbar>
  );
}

export default SnackBar;
