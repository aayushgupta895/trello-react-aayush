import React, { createContext, useState, useContext } from "react";

const MyContext = createContext();

export const MyProvider = ({ children }) => {
  const [snackbarOpen, setSnackbarOpen] = useState({
    open : false,
    message : 'showed'
  });

  const [errMessage, setErrMessage] = useState("")

  return (
    <MyContext.Provider value={{ snackbarOpen, setSnackbarOpen, setErrMessage, errMessage }}>
      {children}
    </MyContext.Provider>
  );
};

export const useMyContext = () => {
  return useContext(MyContext);
};
