import "./App.css";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import BoardDetail from "./pages/BoardDetail";
import ChecklistWrapper from "./components/checklist/ChecklistWrapper";
import SnackBar from "./utils/SnackBar";
import { useMyContext } from "./MyContext";
import Error from "./pages/Error";


function App() {
  const { snackbarOpen, setSnackbarOpen } = useMyContext(); 
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/board/:id" element={<BoardDetail />}>
          <Route path="card/:cardId" element={<ChecklistWrapper />}></Route>
        </Route>
        <Route path="/error" element={<Error />}/>
      </Routes>
      <SnackBar open={snackbarOpen} setOpen={setSnackbarOpen} />
    </>
  );
}

export default App;
