import React from "react";
import "./index.css";
import { ThemeProvider, createTheme, CssBaseline } from "@mui/material";

const theme = createTheme({
  palette: {
    mode: "light",
    text: {
      primary: "hsl(200, 15%, 8%)",
    },
    background: {
      default: "#fff",
      paper: "hsl(0, 0%, 100%)",
    },
  },
  typography: {
    fontSize: 16,
    fontFamily: `"Nunito Sans", sans-serif`,
  },
  spacing: 16,
});

function CustomThemeProvider({ children }) {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
}

export default CustomThemeProvider;
