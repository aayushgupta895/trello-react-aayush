import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
import CustomThemeProvider from "./CustomThemeProvider.jsx";
import { MyProvider } from "./MyContext";

ReactDOM.createRoot(document.getElementById("root")).render(
  <BrowserRouter>
    <CustomThemeProvider>
      <MyProvider>
        <App />
      </MyProvider>
    </CustomThemeProvider>
  </BrowserRouter>
);
